// Set default app background
Titanium.UI.setBackgroundColor('#fafafa');

var navWindow, mainWindow, introWindow;

// Requiring Modules
mainWindow = require('mainWindowView').mainWindow;
introWindow = require('introWindowView');

// Define Navigation Window
navWindow = Ti.UI.iOS.createNavigationWindow({ 
    window : mainWindow
});

// Open Navigation Window
navWindow.open();