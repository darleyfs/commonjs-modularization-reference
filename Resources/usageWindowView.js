// Initialize Usage Window
exports.init = function(){
    var usageWindow, usageIntroLabel, usageExportsTitle, usageExportsCopy,
        usageRequireTitle, usageRequireCopy;
    
    // Usage Window
    usageWindow = Ti.UI.createWindow({
        title : 'Usage',
        backgroundColor : '#fafafa',
        layout : 'vertical'
    });
    
    // Usage window intro copy
    usageIntroLabel = Ti.UI.createLabel({
       text : 'To use CommonJS modules you must first\ncreate a new '
              + 'JavaScript file within your project resources folder. '
              + 'Module file names may contain periods but must not begin '
              + 'with a forward slash (/) or number. Module filenames should '
              + 'also not include the words "require", "exports", or "module".',
       font : {
           fontSize : '16sp'
       },
       color : '#333',
       top : 16, left : 16
    });
    
    // Title for exports section
    usageExportsTitle = Ti.UI.createLabel({
        text : 'Exports',
        font : {
            fontSize : '18dp',
            fontWeight : 'bold'
        },
        color : '#333',
        top : 16, left : 16
    });
    
    // Exports section
    usageExportsCopy = Ti.UI.createLabel({
       text : 'All modules contain a free "exports" variable.\nThe exports '
              + 'variable is an object used to access specific parts within '
              + 'the module.'
              
              + '\n\nNote: The exports variable should only be used '
              + 'for exporting within a CommonJS module.',
       font : {
           fontSize : '16sp'
       },
       color : '#333',
       top : 3, left : 16,    
    });
    
    // Title for require section
    usageRequireTitle = Ti.UI.createLabel({
        text : 'Require',
        font : {
            fontSize : '18dp',
            fontWeight : 'bold'
        },
        color : '#333',
        top : 16, left : 16
    });
    
    // Require section
    usageRequireCopy = Ti.UI.createLabel({
       text : 'To import a module into your application use the require '
              + 'function and pass the module filename, minus the .JS extension '
              + ', as a string argument. eg. require("myData") for myData.js.' 
              
              + '\n\nWhen the require function is assignment to a variable it '
              + 'is re-assignment the value of the modules exports object. '
              + 'For example if you require myData as var data = require("myData") '
              + ' any properties assigned to the exports object in myData are '
              + 'now accessisble via the data variable. eg. exports.data in '
              + 'myData is now accessiable via the data.data property.',
       font : {
           fontSize : '16sp'
       },
       color : '#333',
       top : 3, left : 16,    
    });
    
    
    Ti.API.debug('Adding components to usageWindow');
    usageWindow.add(usageIntroLabel);
    
    usageWindow.add(usageExportsTitle);
    usageWindow.add(usageExportsCopy);
    
    usageWindow.add(usageRequireTitle);
    usageWindow.add(usageRequireCopy);
    
    
    Ti.API.debug('Opening usageWindow');    
    navWindow.openWindow(usageWindow);
};
